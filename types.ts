// Master interface, where everything's optional
// Extending interfaces can then determin what's mandatory
export interface IMasterGeneticConf<Genotype> {
  // Number of critters in each generation
  size?: number;
  // How likely it is that a crossover will happen
  crossoverChance?: number;
  // How likely a new critter is to be mutated
  mutationRate?: number;
  // How many generations to run
  iterations?: number;
  // Should we always take the fittest critter into the next generation
  fittestAlwaysSurvives?: boolean;
  // How many results to send to the notification function
  maxResults?: number;
  // How many generations to skip between each notification
  skip?: number;
  // What % of every generation is completely new critters
  freshBlood?: number;
  // If the best critter hasn't gotten any more fit, exit
  haltOnNoChange?: number;

  // Creates a new critter.  Must be pure
  createCritter?: () => Genotype;
  // Once we get to a fitness score above this number, stop
  targetFitness?: number;
  // If we're trying to match something, this is it
  target?: Genotype;
  // Calculate the fitness for a critter
  calcFitness?: (genes: Genotype) => number;
  // allows the user to see how the algo is doing
  sendNotification?: (pop: IGenePackage<Genotype>[], generation: number, isFinished: boolean) => void;
  // defines how the critters may mutate
  mutate?;
  // defines how two critters are combined
  crossover?;
  // Allows the end user to run against an entire population at once
  calcPopFitness?;
}

// Used internally, this is what needs to be defined in order for the thing to run
export interface IGeneticConf<Genotype> extends IMasterGeneticConf<Genotype> {
  size: number;
  crossoverChance: number;
  mutationRate: number;
  iterations: number;
  maxResults: number;
  skip: number;
  freshBlood: number;
}

// User-facing, they need to define how new critters appear, everything else is optional
export interface IGeneticUserConf<Genotype> extends IMasterGeneticConf<Genotype> {
  createCritter: () => Genotype;
}

export interface IGeneticStats {
  min: number;
  max: number;
  stdev: number;
  mean: number;
}

export interface IGenePackage<Genotype> {
  genes: Genotype;
  fitness: number;
}

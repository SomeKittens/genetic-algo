import { IGenePackage, IGeneticStats } from './types';

export function randNumber(min: number, max?: number): number {
  if (!max) {
    max = min;
    min = 0;
  }
  return Math.floor(Math.random() * (max - min)) + min;
}

export let Optimize = {
  Maximize: (a, b): boolean => a >= b,
  Minimize: (a, b): boolean => a < b
};

import { randNumber, Optimize } from './util';
import { IGenePackage, IGeneticConf, IGeneticStats } from './types';

/**
 * The Population class contains a single round's worth of population
 * as well as ways of selecting critters out of it
 * TODO: currently no way for the user to tell us optimization method
 */
export class Population<Genotype> {
  stats: IGeneticStats;
  critters: IGenePackage<Genotype>[];
  genes: Genotype[];
  private internalGenState: {
    rlr?: number;
    seq?: number;
  };

  // tslint:disable-next-line:member-ordering
  optimize = Optimize.Maximize;

  // TODO: Find better place to put these
  // Tricky bit is maintaining the internal state for Random linear & sequential
  private Select1: {[key: string]: () => Genotype} = {
    Tournament2: () => {
      let n = this.critters.length;
      let a = this.critters[randNumber(n)];
      let b = this.critters[randNumber(n)];
      return this.optimize(a.fitness, b.fitness) ? a.genes : b.genes;
    },
    Tournament3: () => {
      let n = this.critters.length;
      let a = this.critters[randNumber(n)];
      let b = this.critters[randNumber(n)];
      let c = this.critters[randNumber(n)];
      let best = this.optimize(a.fitness, b.fitness) ? a : b;
      best = this.optimize(best.fitness, c.fitness) ? best : c;
      return best.genes;
    },
    Fittest: () => this.critters[0].genes,
    Random: () => {
      return this.critters[randNumber(this.critters.length)].genes;
    },
    RandomLinearRank: () => {
      return this.critters[randNumber(Math.min(this.critters.length, (this.internalGenState.rlr++)))].genes;
    },
    Sequential: () => {
      return this.critters[(this.internalGenState.seq++) % this.critters.length].genes;
    }
  };

  private Select2: {[key: string]: () => Genotype[]} = {
    Tournament2: () =>
      [this.Select1.Tournament2(), this.Select1.Tournament2()],
    Tournament3: () =>
      [this.Select1.Tournament3(), this.Select1.Tournament3()],
    Random: () =>
      [this.Select1.Random(), this.Select1.Random()],
    RandomLinearRank: () =>
      [this.Select1.RandomLinearRank(), this.Select1.RandomLinearRank()],
    Sequential: () =>
      [this.Select1.Sequential(), this.Select1.Sequential()],
    FittestRandom: () =>
      [this.Select1.Fittest(), this.Select1.Random()]
  };

  // tslint:disable-next-line:member-ordering
  selectSingleCritter = this.Select1.RandomLinearRank;

  // tslint:disable-next-line:member-ordering
  selectCritterPair = this.Select2.RandomLinearRank;

  // TODO: I don't like how calcFitness is passed through here
  constructor(private calcFitness, private calcPopFitness) {
    this.internalGenState = {
      rlr: 0,
      seq: 0
    };
  }

  runFitnessCalcOnPop(geneBatch: Genotype[]) {
    let fitnessP;

    if (this.calcFitness) {
      // Each is independent
      fitnessP = Promise.all(
        geneBatch.map(genes => {
          return this.wrapFitness(genes)
            .then(fitness => ({
              genes,
              fitness
            }));
        })
      );
    } else {
      // Must calculate entire population together
      fitnessP = this.wrapPopFitness(geneBatch);
    }
    return fitnessP.then(critters => {
      this.critters = critters.sort((a, b): number => this.optimize(a.fitness, b.fitness) ? -1 : 1);
      this.genes = this.critters.map(c => c.genes);

      // Calc stats about current generation
      let mean = this.critters.reduce((a, b) => a + b.fitness, 0) / this.critters.length;
      let stdev = Math.sqrt(
        this.critters
        .map(a => (a.fitness - mean) * (a.fitness - mean))
        .reduce((a, b) => a + b, 0)
        / this.critters.length
      );

      this.stats = {
        max: this.critters[0].fitness,
        min: this.critters[this.critters.length - 1].fitness,
        mean,
        stdev
      };

      return this;
    });
  }

  private wrapFitness(g): Promise<any> {
    let fn = this.calcFitness(g);
    if (fn.then) {
      return fn;
    } else {
      // it was sync
      return Promise.resolve(fn);
    }
  }

  private wrapPopFitness(g): Promise<any> {
    let fn = this.calcPopFitness(g);
    if (fn.then) {
      return fn;
    } else {
      // it was sync
      return Promise.resolve(fn);
    }
  }
}

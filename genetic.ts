// Heavily modified version of http://subprotocol.com/system/genetic-js.html

import { randNumber, Optimize } from './util';
import { IGenePackage, IGeneticConf, IGeneticStats, IGeneticUserConf } from './types';
import { Population } from './Population';

// Default function assumes Genotype is an array and a target has been set
// explicit any here because when we set this, we've confirmed Genotype is array
// But TS doesn't know that, so it complains
function defaultFitnessFunction (target: any): (genes: any) => number {
  return function calcFitness(genes: any[]): number {
    let score = 0;
    for (let i = 0; i < genes.length; i++) {
      // Is the character correct?
      if (genes[i] === target[i]) {
        // If so, increment the score.
        score++;
      }
    }
    // Fitness is the percentage correct.
    return score / genes.length;
  };
}

// Assumes that Genotype is array and that each gene is independent
function defaultCrossoverFunction(parentA: any[], parentB: any[]): any[] {
  // The child is a new instance of DNA.
  // Note that the DNA is generated randomly in the constructor, but we will overwrite it below with DNA from parents.
  let childA = [];
  let childB = [];

  // Picking a random “midpoint” in the genes array
  let midpoint = randNumber(parentA.length);

  for (let i = 0; i < parentA.length; i++) {
    // Before midpointint copy genes from one parent, after midpoint copy genes from the other parent
    if (i > midpoint) {
      childA[i] = parentA[i];
      childB[i] = parentB[i];
    } else {
      childA[i] = parentB[i];
      childB[i] = parentA[i];
    }
  }
  // Return the new child DNA
  return [childA, childB];
}

// Default mutation function
// Assumes that Genotype is array and that each gene is independent
function defaultMutation(seed) {
  return function mutate(genes) {
    let idx = randNumber(genes.length);
    let patsy = seed();
    genes[idx] = patsy[idx];
    return genes;
  };
}

export class Genetic<Genotype> {
  // Defined later, must be overwritten
  calcFitness: (genes: Genotype) => number;
  calcPopFitness: (genes: Genotype[]) => IGenePackage<Genotype>;

  mutate: (genes: Genotype) => Genotype;
  crossover: (a: Genotype, b: Genotype) => Genotype[];
  createCritter: () => Genotype;

  userData: any;
  roundsWithNoChange = 0;

  entities: Genotype[];

  // Default config, user can override
  configuration: IGeneticConf<Genotype> = {
    size: 250,
    crossoverChance: 0.9,
    mutationRate: 0.2,
    iterations: 1000,
    fittestAlwaysSurvives: true,
    maxResults: 100,
    skip: 10,
    freshBlood: 0.2
  };

  constructor () {
    this.userData = {};
    this.entities = [];
  }

  /**
   * This function actually starts everything off, taking in (and validating) a config object
   *   setting smart defaults where applicable, and finally triggering the algo itself.
   * @param config Configuration for tweaking the genetic variables
   * @param userData allows the user to set arbitrary data on Genetic for all functions to use
   */
  evolve(config: IGeneticUserConf<Genotype>, userData?): any {
    let k;
    Object.assign(this.configuration, config);

    if (this.configuration.freshBlood >= 1) {
      throw new Error('config.freshBlood is too high, must be less than one');
    }
    if (this.configuration.freshBlood < 0) {
      throw new Error('config.freshBlood is too low, must be at least zero');
    }

    if (!this.configuration.createCritter) {
      throw new Error('Must provide createCritter method in config');
    }
    this.createCritter = this.configuration.createCritter;

    // The default calcFitness function makes some assumptions
    // We need to make sure their config conforms to those assumptions
    if (!this.configuration.calcFitness && !this.configuration.calcPopFitness) {
      // Need a set target
      if (!this.configuration.target) {
        throw new Error('Must provide target or custom fitness function');
      }
      // Genotype needs to be an array
      if (!Array.isArray(this.configuration.createCritter())) {
        throw new Error('Must provide array Genotype or custom fitness function');
      }

      // If we've made it this far, the provided config works with the default fitness & mutation
      this.calcFitness = defaultFitnessFunction(this.configuration.target);
      this.mutate = defaultMutation(this.createCritter);
    } else {
      this.calcFitness = this.configuration.calcFitness;
      this.calcPopFitness = this.configuration.calcPopFitness;

      if (Array.isArray(this.configuration.createCritter()) && !this.configuration.mutate) {
        this.mutate = defaultMutation(this.createCritter);
      }
    }

    if (this.configuration.crossoverChance) {
      this.crossover = this.configuration.crossover || defaultCrossoverFunction;
    }

    if (this.configuration.sendNotification) {
      this.sendNotification = this.configuration.sendNotification;
    }

    Object.assign(this.userData, userData);

    // run process
    return this.start();
  }

  // Can be overwritten, don't need to
  isComplete(pop: Population<Genotype>, currentGen: number): boolean {
    if (this.configuration.targetFitness !== undefined) {
      return pop.critters[0].fitness >= this.configuration.targetFitness;
    }
    // Just let it run if we don't know enough
    return false;
  }
  sendNotification(pop: IGenePackage <Genotype>[], generation: number, isFinished: boolean): void {
    let display;
    if (typeof pop[0].genes[0] === 'string') {
      display = (<string[]><any>pop[0].genes).join('');
    } else {
      display = pop[0].genes;
    }
    console.log(`
===== Generation ${generation} =====
Fittest critter: ${display}
Fitness: ${(pop[0].fitness * 100).toLocaleString()}%
`);
  }

  private mutateOrNot(genes: Genotype): Genotype {
    // applies mutation based on mutation probability
    return Math.random() <= this.configuration.mutationRate && this.mutate ?
      // Mutate!
      this.mutate(genes) :
      // Nah, keep as is
      genes;
  }

  private start() {
    let i, best;

    // seed the population
    for (i = 0; i < this.configuration.size; ++i)  {
      this.entities.push(this.createCritter());
    }

    let pop = new Population(this.calcFitness, this.calcPopFitness);
    let generationP = Promise.resolve(this.entities);
    let stopIterating = false;
    for (i = 0; i < this.configuration.iterations; ++i) {
      if (stopIterating) { break; }
      generationP = generationP.then(entities => {
        return pop.runFitnessCalcOnPop(entities);
      })
      .then((subPop) => {

        if (subPop.stats.max <= best) {
          this.roundsWithNoChange++;
        } else {
          best = subPop.stats.max;
          this.roundsWithNoChange = 0;
        }

        let isFinished = this.isComplete(subPop, i) ||
                        i === this.configuration.iterations - 1 ||
                        (this.configuration.haltOnNoChange &&
                        this.roundsWithNoChange >= this.configuration.haltOnNoChange);

        if (
          isFinished || this.configuration.skip === 0 || i % this.configuration.skip === 0
        ) {
          this.sendNotification(subPop.critters.slice(0, this.configuration.maxResults), i, isFinished);
        }

        if (isFinished) {
          stopIterating = true;
          return;
        }

        // crossover and mutate
        let newPop: Genotype[] = [];

        // lets the best solution fall through
        if (this.configuration.fittestAlwaysSurvives) {
          newPop.push(subPop.genes[0]);
        }

        let freshCritters = Math.floor(this.configuration.size * this.configuration.freshBlood);
        while (newPop.length < freshCritters) {
          newPop.push(this.createCritter());
        }

        while (newPop.length < this.configuration.size) {
          if (
            this.crossover // if there is a crossover function
            && Math.random() <= this.configuration.crossoverChance // base crossover on specified probability
            && newPop.length + 1 < this.configuration.size // keeps us from going 1 over the max population size
          ) {
            let parents = subPop.selectCritterPair();
            let children = this.crossover(parents[0], parents[1])
            .map((geneSet: Genotype) => this.mutateOrNot(geneSet));

            newPop.push(children[0], children[1]);
          } else {
            newPop.push(this.mutateOrNot(subPop.selectSingleCritter()));
          }
        }

        // All-new population!  Time to see how they stack up
        this.entities = newPop;
        return this.entities;
      });
    }
    return generationP;
  }
}

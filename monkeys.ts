const NUM_CHARS = 18;
let target = 'to be or not to be';

import {randNumber} from './util';
import {Genetic} from './genetic';

function createCritter(): string[] {
  let genes = [];
  // Picking randomly from a range of characters with ASCII values between 32 and 128
  for (let i = 0; i < NUM_CHARS; i++) {
    genes[i] = String.fromCharCode(randNumber(32, 128));
  }
  return genes;
}

let config = {
  createCritter,
  target: target.split(''),
  targetFitness: 1
};

let monkeyG = new Genetic<string[]>();

monkeyG.evolve(config);

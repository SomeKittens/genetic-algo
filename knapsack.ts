// Implementation of the knapsack problem
// Also includes greedy & weighted greedy solution as benchmark
// Parts pulled from http://burakkanber.com/blog/machine-learning-genetic-algorithms-in-javascript-part-2/

// (best solution is score of 5968 and weight 998)

interface IKnapsackElement {
  name: string;
  weight: number;
  value: number;
}

let elements = require('../knapsackValues.json');
let elementArr: IKnapsackElement[] = Object.keys(elements).map(key => {
  return Object.assign({
    name: key
  }, elements[key]);
});
const MAX_WEIGHT = 1000;


function greedy() {
  let knapsack = [];
  let remainingRoom = MAX_WEIGHT;
  let sortedElements = elementArr
  .sort((a, b) => b.value - a.value)
  .forEach(element => {
    if (remainingRoom > element.weight) {
      remainingRoom -= element.weight;
      knapsack.push(element);
    }
  });

  return knapsack;
}

function weightedGreedy() {
  let knapsack = [];
  let remainingRoom = MAX_WEIGHT;
  let sortedElements = elementArr
  .sort((a, b) => (b.value / b.weight) - (a.value / a.weight))
  .forEach(element => {
    if (remainingRoom > element.weight) {
      remainingRoom -= element.weight;
      knapsack.push(element);
    }
  });

  return knapsack;
}
let prevBest;
function printScore(type: string, knapsack, fitness?, isDone?: boolean) {
  if (!fitness) {
    fitness = (knapsack.reduce((acc, el) => acc + el.value, 0)).toLocaleString();
  }
  if (prevBest === fitness && !isDone) { return; }
  prevBest = fitness;
  console.log(`===== ${type} =====`);
  console.log('value: ', (knapsack.reduce((acc, el) => acc + el.value, 0)).toLocaleString());
  console.log('space: ', (knapsack.reduce((acc, el) => acc + el.weight, 0)).toLocaleString());
  console.log('score: ', fitness.toLocaleString());
  // console.log(knapsack);
  console.log();
}

// printScore('greedy', greedy());
// printScore('weightedGreedy', weightedGreedy());

// ---- Actual algo API starts here ---- //

// gene is represented as an array of zeros and ones
// zero means we don't take element at that index, one means we do

import {randNumber} from './util';
import {Genetic} from './genetic';
import { IGeneticUserConf } from './types';

function createCritter(): string[] {
  let genes = [];
  // Picking randomly from a range of characters with ASCII values between 32 and 128
  for (let i = 0; i < elementArr.length; i++) {
    genes[i] = Math.random() > 0.5 ? 1 : 0;
  }
  return genes;
}

function genesToKnapsack(genes: number[]): IKnapsackElement[] {
  return genes.map((g, idx) => {
    if (g) {
      return elementArr[idx];
    }
  })
  .filter(Boolean);
}

function calcFitness(genes) {
  let knapsack = genesToKnapsack(genes);
  let score = knapsack.reduce((acc, el) => acc + el.value, 0);
  let weight = knapsack.reduce((acc, el) => acc + el.weight, 0);

  if (weight > MAX_WEIGHT) {
    // the more overweight you are, the heavier each pound penalizes you
    // BUT we don't want things that are a smidge over
    // Assign whatever has the bigger penalty
    return score - Math.max(
      Math.pow(weight - MAX_WEIGHT, 2),
      (weight - MAX_WEIGHT) * 50
    );
  }
  return score;
}

let scores = [];

function sendNotification(pop, gen, isFinished) {
  printScore('Generation ' + gen, genesToKnapsack(pop[0].genes), calcFitness(pop[0].genes));
  if (isFinished) {
    console.log('===== DONE =====');
    printScore('Generation ' + gen, genesToKnapsack(pop[0].genes),calcFitness(pop[0].genes), true);
  }
}

// Somehow mutation causes excessive weight gain
// Disabled for now
function mutate(genes) {
  let idx = randNumber(genes.length);
  genes[idx] = genes[idx] === 0 ? 1 : 0;
  return genes;
}

let config: IGeneticUserConf<string[]> = {
  createCritter,
  calcFitness,
  // mutate,
  sendNotification
};

let knapG = new Genetic<string[]>();
knapG.evolve(config);
